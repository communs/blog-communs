<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="page" role="main">
	<?php get_sidebar(); ?>

	<article class="main-content">
		<div class="small-12 row column"><div class="separator"></div></div>
		<?php if ( have_posts() ) : ?>

		<div class="row" id="sidebar-top-position">
			<div id="masonry-grid"  >
				<?php /* Start the Loop */ ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<div class="block-item column">
						<?php get_template_part( 'template-parts/content', get_post_format() ); ?>
					</div>
				<?php endwhile; ?>

				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>

				<?php endif; // End have_posts() check. ?>
			</div>
		</div>

		<?php /* Display navigation to next/previous pages when applicable */ ?>
		<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
			<nav id="post-nav">
				<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
				<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
			</nav>
		<?php } ?>

	</article>
	

</div>
<script>
	$(window).load(function(){
	  $('#masonry-grid').masonry({
	    itemSelector: '#masonry-grid .block-item'
	  });
	});
</script>
<?php get_footer();
